filter_packages = [
     ("^(BaseOS|AppStream|CRB|HighAvailability|NFV|ResilientStorage)$", {
         "*": [
             "kernel-rt*", #RhBug 1973568
             "javapackages-bootstrap", #CS-636
         ]
     }),

     ("^.*$", {
         "*": [
             "glibc32",
             "libgcc32",
             "scap-security-guide-rule-playbooks",
             "*openh264*",      # https://fedoraproject.org/wiki/Non-distributable-rpms
             "python3-openipmi", #RhBug 1982794
             "OpenIPMI-perl", #RhBug 1982794
         ]
         "ppc64le": [
             "SLOF",
             "guestfs-tools",
             "libguestfs",
             "libvirt-daemon-kvm",
             "libvirt-daemon-driver-qemu",
             "qemu-kiwi",
             "qemu-kvm",
             "supermin",
             "virt-manager",
             "virt-v2v",
             "virt-p2v",
             "virt-top",
             "cockpit-machines",
         ],
       "s390x": [
             "rust-std-static-wasm32-unknown-unknown", #ENGCMP-1255
             "rust-std-static-wasm32-wasi",
         ],
     }),

]


additional_packages = [
    # Everything contains everything.
    ('^Everything$', {
        '*': [
            '*',
        ],
    }),
    ("^BaseOS$", {
        "*": [
            "liblockfile", #ENGCMP-2535
            "python3-gobject-base-noarch", #ENGCMP-2400
            "python3.*-debuginfo", #ENGCMP-1433
            "subscription-manager-cockpit", #ENGCMP-2427
            "subscription-manager-rhsm-certificates", #ENGCMP-2357
        ]
    }),
    ("^BaseOS$", {
         "aarch64": [
         ],
         "ppc64le": [
         ],
         "x86_64": [
         ],
         "s390x": [
         ],
     }),
    ("^AppStream$", {
        "*": [
            "aardvark-dns", #ENGCMP-2515
            "adobe-source-code-pro-fonts", #ENGCMP-2390
            "alsa-plugins-pulseaudio", #ENGCMP-2359
            "aspnetcore-runtime-7.0", #ENGCMP-2586
            "aspnetcore-targeting-pack-7.0", #ENGCMP-2586
            "capstone", #ENGCMP-2591
            "dotnet-apphost-pack-7.0", #ENGCMP-2586
            "dotnet-host", #ENGCMP-2586
            "dotnet-hostfxr-7.0", #ENGCMP-2586
            "dotnet-runtime-7.0", #ENGCMP-2586
            "dotnet-sdk-7.0", #ENGCMP-2586
            "dotnet-targeting-pack-7.0", #ENGCMP-2586
            "dotnet-templates-7.0", #ENGCMP-2586
            "dpdk-devel", #ENGCMP-2205
            "egl-utils", #ENGCMP-2476
            "freeglut-devel", #ENGCMP-2073
            "frr-selinux", #ENGCMP-2697
            "gcc-toolset-12", #ENGCMP-2391
            "gcc-toolset-12-annobin-annocheck", #ENGCMP-2384
            "gcc-toolset-12-annobin-docs", #ENGCMP-2384
            "gcc-toolset-12-annobin-plugin-gcc", #ENGCMP-2384
            "gcc-toolset-12-binutils", #ENGCMP-2415
            "gcc-toolset-12-binutils-devel", #ENGCMP-2415
            "gcc-toolset-12-binutils-gold", #ENGCMP-2415
            "gcc-toolset-12-build", #ENGCMP-2391
            "gcc-toolset-12-dwz", #ENGCMP-2402
            "gcc-toolset-12-gcc", #ENGCMP-2405
            "gcc-toolset-12-gcc-c++", #ENGCMP-2405 
            "gcc-toolset-12-gcc-gfortran", #ENGCMP-2405 
            "gcc-toolset-12-gcc-plugin-devel", #ENGCMP-2405 
            "gcc-toolset-12-gdb", #ENGCMP-2416
            "gcc-toolset-12-gdbserver", #ENGCMP-2416
            "gcc-toolset-12-libasan-devel", #ENGCMP-2405 
            "gcc-toolset-12-libatomic-devel", #ENGCMP-2405 
            "gcc-toolset-12-libgccjit", #ENGCMP-2405 
            "gcc-toolset-12-libgccjit-devel", #ENGCMP-2405 
            "gcc-toolset-12-libgccjit-docs", #ENGCMP-2405 
            "gcc-toolset-12-libitm-devel", #ENGCMP-2405 
            "gcc-toolset-12-liblsan-devel", #ENGCMP-2405 
            "gcc-toolset-12-libquadmath-devel", #ENGCMP-2405 
            "gcc-toolset-12-libstdc++-devel", #ENGCMP-2405 
            "gcc-toolset-12-libstdc++-docs", #ENGCMP-2405 
            "gcc-toolset-12-libtsan-devel", #ENGCMP-2405 
            "gcc-toolset-12-libubsan-devel", #ENGCMP-2405 
            "gcc-toolset-12-offload-nvptx", #ENGCMP-2405 
            "gcc-toolset-12-runtime", #ENGCMP-2391
            "gnome-kiosk-script-session", #ENGCMP-2499
            "gnome-kiosk-search-appliance", #ENGCMP-2499
            "ignition-validate", #ENGCMP-2656
            "keylime", #ENGCMP-2419
            "keylime-agent-rust", #ENGCMP-2420
            "keylime-base", #ENGCMP-2419
            "keylime-registrar", #ENGCMP-2419
            "keylime-selinux", #CS-1194
            "keylime-tenant", #ENGCMP-2419
            "keylime-verifier", #ENGCMP-2419
            "libasan8", #ENGCMP-2405
            "libgpiod", #ENGCMP-2433
            "libgpiod-devel", #ENGCMP-2433
            "libgpiod-utils", #ENGCMP-2433
            "libi2cd", #ENGCMP-2428
            "libi2cd-devel", #ENGCMP-2428
            "libnxz", #ENGCMP-2576
            "libsepol-utils", #ENGCMP-2399
            "libtsan2", #ENGCMP-2405 
            "libzdnn", #ENGCMP-2244
            "libzdnn-devel", #ENGCMP-2297
            "man-db-cron", #ENGCMP-2595
            "mkpasswd", #ENGCMP-2259
            "netavark", #ENGCMP-2543
            "netstandard-targeting-pack-2.1", #ENGCMP-2586
            "nfsv4-client-utils", #ENGCMP-2493
            "nvme-stas", #ENGCMP-2495
            "libnvme", #ENGCMP-2358
            "poppler-qt5", #ENGCMP-2393
            "python3-dnf-plugin-modulesync", #ENGCMP-2323
            "python3-alembic", #ENGCMP-2424
            "python3-greenlet", #ENGCMP-2421
            "python3-keylime", #ENGCMP-2419
            "python3-lark-parser", #ENGCMP-2422
            "python3-i2c-tools", #RHBZ#2072719
            "python3-libgpiod", #ENGCMP-2433
            "python3-libnvme", #ENGCMP-2412
            "python3-pyqt5-sip", #ENGCMP-2370
            "python3-sqlalchemy", #ENGCMP-2423
            "python3-tornado", #ENGCMP-2418
            "python3-virt-firmware", #ENGCMP-2726
            "python3-wcwidth", #ENGCMP-2093
            "qatlib-service", #ENGCMP-2490
            "redhat-cloud-client-configuration", #ENGCMP-2401
            "sip6", #ENGCMP-2239
            "sssd-idp", #ENGCMP-2276
            "system-backgrounds",
            "tuned-profiles-postgresql", #ENGCMP-2126
            "usbredir-server", #ENGCMP-2719
            "xdg-desktop-portal-gnome", #ENGCMP-2146
            "xmlstarlet", #ENGCMP-2296
            "xxhash", #ENGCMP-2455
            "xxhash-libs", #ENGCMP-2455
            "yara", #ENGCMP-2372
        ]
    }),
    ("^AppStream$", {
         "x86_64": [
             "open-vm-tools-salt-minion", #ENGCMP-2295
             "virt-dib",
             "vorbis-tools",
         ],
         "aarch64": [
             "virt-dib",
         ],
         "s390x": [
             "virt-dib",
         ],
         "ppc64le": [
             "vorbis-tools",
         ],

     }),
    ("^CRB$", {
        "*": [
         ], 
        "x86_64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "aarch64": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "s390x": [
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "dotnet-sdk-6.0-source-built-artifacts", #CS-1025
         ],
         "ppc64le": [
             "java-1.8.0-openjdk-*slowdebug*", #ENGCMP-1327
             "java-11-openjdk-*slowdebug*", #ENGCMP-1327
             "java-17-openjdk-*slowdebug*", #ENGCMP-1326
             "java-1.8.0-openjdk-*fastdebug*", #ENGCMP-1327
             "java-11-openjdk-*fastdebug*", #ENGCMP-1327
             "java-17-openjdk-*fastdebug*", #ENGCMP-1326
         ],
     }),

    ("^Buildroot$", {
        "*": [
            "*",
        ]
    }),
]
